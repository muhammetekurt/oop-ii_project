﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Drawing;

namespace OOP_2_OnlineBookStore
{

    class MusicCDProvider
    {
        SqlConnection sqlCon;
        SqlCommand cmd;
        string query;

        string connectionString = "Data Source = SQL5097.site4now.net; Initial Catalog = db_a750b9_projeodevi2; User Id = db_a750b9_projeodevi2_admin; Password = admin2626";
        /**
        * parametresiz constructor
        */
        public MusicCDProvider()
        {
            sqlCon = new SqlConnection(connectionString);
        }
        /**
        * Veritabanından Muzık urunlerının degerlerini okuyarak oluşturulan musicCDlist icerisine kaydediyor.
        */
        public List<MusicCD> musicCDlistFromDatabase()  
        {
            List<MusicCD> musicCDlist = new List<MusicCD>();
            query = "SELECT id, name, singer, type, price,images, stock, soldCount FROM Product where product_type=3";
            sqlCon.Open();
            cmd = new SqlCommand(query, sqlCon);
            SqlDataReader dr = cmd.ExecuteReader();
            

            while (dr.Read())
            {
                MusicCD cd = new MusicCD(); 

                cd.id = Convert.ToInt32(dr[0]);        
                cd.name = dr[1].ToString();           
                cd.Singer = dr[2].ToString();
                cd.Type = dr[3].ToString();
                cd.price = Convert.ToInt32(dr[4]);

                byte[] picture = new byte[0];  

                if (dr[5] == DBNull.Value)
                {
                    picture = null;                    
                }
                else
                {                    
                    picture = (byte[])dr[5];                   
                }
                cd.image = picture;

                int stc ;
                if (dr[6] == DBNull.Value)
                {
                    stc = 0;                                     
                }
                else
                {                    
                    stc = Convert.ToInt32(dr[6]);                   
                }
                cd.stock = stc;

                int cnt;
                if (dr[7] == DBNull.Value)
                {
                    cnt = 0;                    
                }
                else
                {                    
                    cnt = Convert.ToInt32(dr[7]);                    
                }
                cd.soldCount = cnt;
                

                musicCDlist.Add(cd);   

            }
            sqlCon.Close();

            return musicCDlist;
        }


       


    }
}
