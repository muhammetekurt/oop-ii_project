﻿
namespace OOP_2_OnlineBookStore
{
    partial class frmAdminPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAdminPanel = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageUsers = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_u_attention = new System.Windows.Forms.Label();
            this.masked_txt_phone = new System.Windows.Forms.MaskedTextBox();
            this.checkBox_isAdmin = new System.Windows.Forms.CheckBox();
            this.txt_u_password = new System.Windows.Forms.TextBox();
            this.lbl_u_password = new System.Windows.Forms.Label();
            this.lbl_u_phone = new System.Windows.Forms.Label();
            this.txt_u_username = new System.Windows.Forms.TextBox();
            this.lbl_u_username = new System.Windows.Forms.Label();
            this.txt_u_address = new System.Windows.Forms.TextBox();
            this.lbl_u_address = new System.Windows.Forms.Label();
            this.txt_u_surname = new System.Windows.Forms.TextBox();
            this.lbl_u_surname = new System.Windows.Forms.Label();
            this.txt_u_name = new System.Windows.Forms.TextBox();
            this.lbl_u_name = new System.Windows.Forms.Label();
            this.dgvUsers = new System.Windows.Forms.DataGridView();
            this.tabPageBooks = new System.Windows.Forms.TabPage();
            this.picBooks = new System.Windows.Forms.PictureBox();
            this.lbl_book_sold = new System.Windows.Forms.Label();
            this.lbl_book_stock = new System.Windows.Forms.Label();
            this.lbl_book_price = new System.Windows.Forms.Label();
            this.lbl_book_page = new System.Windows.Forms.Label();
            this.lbl_book_ISBN = new System.Windows.Forms.Label();
            this.lbl_book_publisher = new System.Windows.Forms.Label();
            this.lbl_book_author = new System.Windows.Forms.Label();
            this.lbl_book_name = new System.Windows.Forms.Label();
            this.txt_book_sold = new System.Windows.Forms.TextBox();
            this.txt_book_stock = new System.Windows.Forms.TextBox();
            this.txt_book_price = new System.Windows.Forms.TextBox();
            this.txt_book_page = new System.Windows.Forms.TextBox();
            this.txt_book_ISBN = new System.Windows.Forms.TextBox();
            this.txt_book_publisher = new System.Windows.Forms.TextBox();
            this.txt_book_author = new System.Windows.Forms.TextBox();
            this.txt_book_name = new System.Windows.Forms.TextBox();
            this.dgvBooks = new System.Windows.Forms.DataGridView();
            this.asd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageMagazines = new System.Windows.Forms.TabPage();
            this.picMag = new System.Windows.Forms.PictureBox();
            this.combo_mag_type = new System.Windows.Forms.ComboBox();
            this.lbl_mag_type = new System.Windows.Forms.Label();
            this.lbl_mag_sold = new System.Windows.Forms.Label();
            this.lbl_mag_stock = new System.Windows.Forms.Label();
            this.lbl_mag_price = new System.Windows.Forms.Label();
            this.lbl_mag_issue = new System.Windows.Forms.Label();
            this.lbl_mag_name = new System.Windows.Forms.Label();
            this.txt_mag_sold = new System.Windows.Forms.TextBox();
            this.txt_mag_stock = new System.Windows.Forms.TextBox();
            this.txt_mag_price = new System.Windows.Forms.TextBox();
            this.txt_mag_issue = new System.Windows.Forms.TextBox();
            this.txt_mag_name = new System.Windows.Forms.TextBox();
            this.dgvMagazines = new System.Windows.Forms.DataGridView();
            this.tabPageMusicCD = new System.Windows.Forms.TabPage();
            this.picMusic = new System.Windows.Forms.PictureBox();
            this.combo_music_type = new System.Windows.Forms.ComboBox();
            this.txt_music_sold = new System.Windows.Forms.TextBox();
            this.lbl_music_sold = new System.Windows.Forms.Label();
            this.txt_music_stock = new System.Windows.Forms.TextBox();
            this.lbl_music_stock = new System.Windows.Forms.Label();
            this.txt_music_price = new System.Windows.Forms.TextBox();
            this.lbl_music_price = new System.Windows.Forms.Label();
            this.txt_music_singer = new System.Windows.Forms.TextBox();
            this.lbl_music_type = new System.Windows.Forms.Label();
            this.lbl_music_singer = new System.Windows.Forms.Label();
            this.txt_music_name = new System.Windows.Forms.TextBox();
            this.lbl_music_name = new System.Windows.Forms.Label();
            this.dgvMusicCD = new System.Windows.Forms.DataGridView();
            this.images = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnChoosePic = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPageUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsers)).BeginInit();
            this.tabPageBooks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBooks)).BeginInit();
            this.tabPageMagazines.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMagazines)).BeginInit();
            this.tabPageMusicCD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMusic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMusicCD)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAdminPanel
            // 
            this.lblAdminPanel.AutoSize = true;
            this.lblAdminPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAdminPanel.Location = new System.Drawing.Point(29, 9);
            this.lblAdminPanel.Name = "lblAdminPanel";
            this.lblAdminPanel.Size = new System.Drawing.Size(118, 24);
            this.lblAdminPanel.TabIndex = 0;
            this.lblAdminPanel.Text = "Admin Panel";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageUsers);
            this.tabControl1.Controls.Add(this.tabPageBooks);
            this.tabControl1.Controls.Add(this.tabPageMagazines);
            this.tabControl1.Controls.Add(this.tabPageMusicCD);
            this.tabControl1.Location = new System.Drawing.Point(12, 48);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(980, 445);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPageUsers
            // 
            this.tabPageUsers.Controls.Add(this.label1);
            this.tabPageUsers.Controls.Add(this.lbl_u_attention);
            this.tabPageUsers.Controls.Add(this.masked_txt_phone);
            this.tabPageUsers.Controls.Add(this.checkBox_isAdmin);
            this.tabPageUsers.Controls.Add(this.txt_u_password);
            this.tabPageUsers.Controls.Add(this.lbl_u_password);
            this.tabPageUsers.Controls.Add(this.lbl_u_phone);
            this.tabPageUsers.Controls.Add(this.txt_u_username);
            this.tabPageUsers.Controls.Add(this.lbl_u_username);
            this.tabPageUsers.Controls.Add(this.txt_u_address);
            this.tabPageUsers.Controls.Add(this.lbl_u_address);
            this.tabPageUsers.Controls.Add(this.txt_u_surname);
            this.tabPageUsers.Controls.Add(this.lbl_u_surname);
            this.tabPageUsers.Controls.Add(this.txt_u_name);
            this.tabPageUsers.Controls.Add(this.lbl_u_name);
            this.tabPageUsers.Controls.Add(this.dgvUsers);
            this.tabPageUsers.Location = new System.Drawing.Point(4, 22);
            this.tabPageUsers.Name = "tabPageUsers";
            this.tabPageUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUsers.Size = new System.Drawing.Size(972, 419);
            this.tabPageUsers.TabIndex = 0;
            this.tabPageUsers.Text = "Users";
            this.tabPageUsers.UseVisualStyleBackColor = true;
            this.tabPageUsers.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(335, 389);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "label1";
            // 
            // lbl_u_attention
            // 
            this.lbl_u_attention.AutoSize = true;
            this.lbl_u_attention.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_u_attention.ForeColor = System.Drawing.Color.Red;
            this.lbl_u_attention.Location = new System.Drawing.Point(321, 316);
            this.lbl_u_attention.Name = "lbl_u_attention";
            this.lbl_u_attention.Size = new System.Drawing.Size(62, 16);
            this.lbl_u_attention.TabIndex = 6;
            this.lbl_u_attention.Tag = "";
            this.lbl_u_attention.Text = "Attention!";
            // 
            // masked_txt_phone
            // 
            this.masked_txt_phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.masked_txt_phone.Location = new System.Drawing.Point(83, 387);
            this.masked_txt_phone.Mask = "(999) 000-0000";
            this.masked_txt_phone.Name = "masked_txt_phone";
            this.masked_txt_phone.Size = new System.Drawing.Size(100, 22);
            this.masked_txt_phone.TabIndex = 5;
            // 
            // checkBox_isAdmin
            // 
            this.checkBox_isAdmin.AutoSize = true;
            this.checkBox_isAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.checkBox_isAdmin.Location = new System.Drawing.Point(324, 335);
            this.checkBox_isAdmin.Name = "checkBox_isAdmin";
            this.checkBox_isAdmin.Size = new System.Drawing.Size(131, 20);
            this.checkBox_isAdmin.TabIndex = 4;
            this.checkBox_isAdmin.Text = "Check, if  isAdmin";
            this.checkBox_isAdmin.UseVisualStyleBackColor = true;
            // 
            // txt_u_password
            // 
            this.txt_u_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_u_password.Location = new System.Drawing.Point(397, 276);
            this.txt_u_password.Name = "txt_u_password";
            this.txt_u_password.Size = new System.Drawing.Size(100, 22);
            this.txt_u_password.TabIndex = 3;
            // 
            // lbl_u_password
            // 
            this.lbl_u_password.AutoSize = true;
            this.lbl_u_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_u_password.Location = new System.Drawing.Point(321, 279);
            this.lbl_u_password.Name = "lbl_u_password";
            this.lbl_u_password.Size = new System.Drawing.Size(70, 16);
            this.lbl_u_password.TabIndex = 2;
            this.lbl_u_password.Text = "password:";
            // 
            // lbl_u_phone
            // 
            this.lbl_u_phone.AutoSize = true;
            this.lbl_u_phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_u_phone.Location = new System.Drawing.Point(16, 387);
            this.lbl_u_phone.Name = "lbl_u_phone";
            this.lbl_u_phone.Size = new System.Drawing.Size(50, 16);
            this.lbl_u_phone.TabIndex = 2;
            this.lbl_u_phone.Text = "Phone:";
            // 
            // txt_u_username
            // 
            this.txt_u_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_u_username.Location = new System.Drawing.Point(397, 248);
            this.txt_u_username.Name = "txt_u_username";
            this.txt_u_username.Size = new System.Drawing.Size(100, 22);
            this.txt_u_username.TabIndex = 3;
            // 
            // lbl_u_username
            // 
            this.lbl_u_username.AutoSize = true;
            this.lbl_u_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_u_username.Location = new System.Drawing.Point(320, 251);
            this.lbl_u_username.Name = "lbl_u_username";
            this.lbl_u_username.Size = new System.Drawing.Size(71, 16);
            this.lbl_u_username.TabIndex = 2;
            this.lbl_u_username.Text = "username:";
            // 
            // txt_u_address
            // 
            this.txt_u_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_u_address.Location = new System.Drawing.Point(83, 304);
            this.txt_u_address.Multiline = true;
            this.txt_u_address.Name = "txt_u_address";
            this.txt_u_address.Size = new System.Drawing.Size(163, 61);
            this.txt_u_address.TabIndex = 3;
            // 
            // lbl_u_address
            // 
            this.lbl_u_address.AutoSize = true;
            this.lbl_u_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_u_address.Location = new System.Drawing.Point(16, 307);
            this.lbl_u_address.Name = "lbl_u_address";
            this.lbl_u_address.Size = new System.Drawing.Size(62, 16);
            this.lbl_u_address.TabIndex = 2;
            this.lbl_u_address.Text = "Address:";
            // 
            // txt_u_surname
            // 
            this.txt_u_surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_u_surname.Location = new System.Drawing.Point(83, 276);
            this.txt_u_surname.Name = "txt_u_surname";
            this.txt_u_surname.Size = new System.Drawing.Size(123, 22);
            this.txt_u_surname.TabIndex = 3;
            // 
            // lbl_u_surname
            // 
            this.lbl_u_surname.AutoSize = true;
            this.lbl_u_surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_u_surname.Location = new System.Drawing.Point(12, 279);
            this.lbl_u_surname.Name = "lbl_u_surname";
            this.lbl_u_surname.Size = new System.Drawing.Size(65, 16);
            this.lbl_u_surname.TabIndex = 2;
            this.lbl_u_surname.Text = "Surname:";
            // 
            // txt_u_name
            // 
            this.txt_u_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_u_name.Location = new System.Drawing.Point(83, 248);
            this.txt_u_name.Name = "txt_u_name";
            this.txt_u_name.Size = new System.Drawing.Size(123, 22);
            this.txt_u_name.TabIndex = 3;
            // 
            // lbl_u_name
            // 
            this.lbl_u_name.AutoSize = true;
            this.lbl_u_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_u_name.Location = new System.Drawing.Point(29, 251);
            this.lbl_u_name.Name = "lbl_u_name";
            this.lbl_u_name.Size = new System.Drawing.Size(48, 16);
            this.lbl_u_name.TabIndex = 2;
            this.lbl_u_name.Text = "Name:";
            // 
            // dgvUsers
            // 
            this.dgvUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsers.Location = new System.Drawing.Point(6, 6);
            this.dgvUsers.Name = "dgvUsers";
            this.dgvUsers.RowHeadersWidth = 51;
            this.dgvUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUsers.Size = new System.Drawing.Size(960, 221);
            this.dgvUsers.TabIndex = 0;
            this.dgvUsers.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUsers_CellClick);
            // 
            // tabPageBooks
            // 
            this.tabPageBooks.Controls.Add(this.picBooks);
            this.tabPageBooks.Controls.Add(this.lbl_book_sold);
            this.tabPageBooks.Controls.Add(this.lbl_book_stock);
            this.tabPageBooks.Controls.Add(this.lbl_book_price);
            this.tabPageBooks.Controls.Add(this.lbl_book_page);
            this.tabPageBooks.Controls.Add(this.lbl_book_ISBN);
            this.tabPageBooks.Controls.Add(this.lbl_book_publisher);
            this.tabPageBooks.Controls.Add(this.lbl_book_author);
            this.tabPageBooks.Controls.Add(this.lbl_book_name);
            this.tabPageBooks.Controls.Add(this.txt_book_sold);
            this.tabPageBooks.Controls.Add(this.txt_book_stock);
            this.tabPageBooks.Controls.Add(this.txt_book_price);
            this.tabPageBooks.Controls.Add(this.txt_book_page);
            this.tabPageBooks.Controls.Add(this.txt_book_ISBN);
            this.tabPageBooks.Controls.Add(this.txt_book_publisher);
            this.tabPageBooks.Controls.Add(this.txt_book_author);
            this.tabPageBooks.Controls.Add(this.txt_book_name);
            this.tabPageBooks.Controls.Add(this.dgvBooks);
            this.tabPageBooks.Location = new System.Drawing.Point(4, 22);
            this.tabPageBooks.Name = "tabPageBooks";
            this.tabPageBooks.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBooks.Size = new System.Drawing.Size(972, 419);
            this.tabPageBooks.TabIndex = 1;
            this.tabPageBooks.Text = "Books";
            this.tabPageBooks.UseVisualStyleBackColor = true;
            this.tabPageBooks.Click += new System.EventHandler(this.tabPageBooks_Click);
            // 
            // picBooks
            // 
            this.picBooks.Location = new System.Drawing.Point(699, 6);
            this.picBooks.Name = "picBooks";
            this.picBooks.Size = new System.Drawing.Size(270, 225);
            this.picBooks.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBooks.TabIndex = 3;
            this.picBooks.TabStop = false;
            // 
            // lbl_book_sold
            // 
            this.lbl_book_sold.AutoSize = true;
            this.lbl_book_sold.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_book_sold.Location = new System.Drawing.Point(242, 310);
            this.lbl_book_sold.Name = "lbl_book_sold";
            this.lbl_book_sold.Size = new System.Drawing.Size(39, 16);
            this.lbl_book_sold.TabIndex = 2;
            this.lbl_book_sold.Text = "Sold:";
            // 
            // lbl_book_stock
            // 
            this.lbl_book_stock.AutoSize = true;
            this.lbl_book_stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_book_stock.Location = new System.Drawing.Point(243, 285);
            this.lbl_book_stock.Name = "lbl_book_stock";
            this.lbl_book_stock.Size = new System.Drawing.Size(45, 16);
            this.lbl_book_stock.TabIndex = 2;
            this.lbl_book_stock.Text = "Stock:";
            // 
            // lbl_book_price
            // 
            this.lbl_book_price.AutoSize = true;
            this.lbl_book_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_book_price.Location = new System.Drawing.Point(242, 257);
            this.lbl_book_price.Name = "lbl_book_price";
            this.lbl_book_price.Size = new System.Drawing.Size(42, 16);
            this.lbl_book_price.TabIndex = 2;
            this.lbl_book_price.Text = "Price:";
            // 
            // lbl_book_page
            // 
            this.lbl_book_page.AutoSize = true;
            this.lbl_book_page.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_book_page.Location = new System.Drawing.Point(49, 369);
            this.lbl_book_page.Name = "lbl_book_page";
            this.lbl_book_page.Size = new System.Drawing.Size(44, 16);
            this.lbl_book_page.TabIndex = 2;
            this.lbl_book_page.Text = "Page:";
            // 
            // lbl_book_ISBN
            // 
            this.lbl_book_ISBN.AutoSize = true;
            this.lbl_book_ISBN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_book_ISBN.Location = new System.Drawing.Point(51, 341);
            this.lbl_book_ISBN.Name = "lbl_book_ISBN";
            this.lbl_book_ISBN.Size = new System.Drawing.Size(42, 16);
            this.lbl_book_ISBN.TabIndex = 2;
            this.lbl_book_ISBN.Text = "ISBN:";
            // 
            // lbl_book_publisher
            // 
            this.lbl_book_publisher.AutoSize = true;
            this.lbl_book_publisher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_book_publisher.Location = new System.Drawing.Point(26, 313);
            this.lbl_book_publisher.Name = "lbl_book_publisher";
            this.lbl_book_publisher.Size = new System.Drawing.Size(67, 16);
            this.lbl_book_publisher.TabIndex = 2;
            this.lbl_book_publisher.Text = "Publisher:";
            // 
            // lbl_book_author
            // 
            this.lbl_book_author.AutoSize = true;
            this.lbl_book_author.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_book_author.Location = new System.Drawing.Point(44, 285);
            this.lbl_book_author.Name = "lbl_book_author";
            this.lbl_book_author.Size = new System.Drawing.Size(49, 16);
            this.lbl_book_author.TabIndex = 2;
            this.lbl_book_author.Text = "Author:";
            // 
            // lbl_book_name
            // 
            this.lbl_book_name.AutoSize = true;
            this.lbl_book_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_book_name.Location = new System.Drawing.Point(45, 257);
            this.lbl_book_name.Name = "lbl_book_name";
            this.lbl_book_name.Size = new System.Drawing.Size(48, 16);
            this.lbl_book_name.TabIndex = 2;
            this.lbl_book_name.Text = "Name:";
            // 
            // txt_book_sold
            // 
            this.txt_book_sold.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_book_sold.Location = new System.Drawing.Point(290, 307);
            this.txt_book_sold.Name = "txt_book_sold";
            this.txt_book_sold.Size = new System.Drawing.Size(100, 22);
            this.txt_book_sold.TabIndex = 1;
            // 
            // txt_book_stock
            // 
            this.txt_book_stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_book_stock.Location = new System.Drawing.Point(290, 279);
            this.txt_book_stock.Name = "txt_book_stock";
            this.txt_book_stock.Size = new System.Drawing.Size(100, 22);
            this.txt_book_stock.TabIndex = 1;
            // 
            // txt_book_price
            // 
            this.txt_book_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_book_price.Location = new System.Drawing.Point(290, 254);
            this.txt_book_price.Name = "txt_book_price";
            this.txt_book_price.Size = new System.Drawing.Size(100, 22);
            this.txt_book_price.TabIndex = 1;
            // 
            // txt_book_page
            // 
            this.txt_book_page.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_book_page.Location = new System.Drawing.Point(99, 366);
            this.txt_book_page.Name = "txt_book_page";
            this.txt_book_page.Size = new System.Drawing.Size(100, 22);
            this.txt_book_page.TabIndex = 1;
            // 
            // txt_book_ISBN
            // 
            this.txt_book_ISBN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_book_ISBN.Location = new System.Drawing.Point(99, 338);
            this.txt_book_ISBN.Name = "txt_book_ISBN";
            this.txt_book_ISBN.Size = new System.Drawing.Size(100, 22);
            this.txt_book_ISBN.TabIndex = 1;
            // 
            // txt_book_publisher
            // 
            this.txt_book_publisher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_book_publisher.Location = new System.Drawing.Point(99, 310);
            this.txt_book_publisher.Name = "txt_book_publisher";
            this.txt_book_publisher.Size = new System.Drawing.Size(100, 22);
            this.txt_book_publisher.TabIndex = 1;
            // 
            // txt_book_author
            // 
            this.txt_book_author.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_book_author.Location = new System.Drawing.Point(99, 282);
            this.txt_book_author.Name = "txt_book_author";
            this.txt_book_author.Size = new System.Drawing.Size(100, 22);
            this.txt_book_author.TabIndex = 1;
            // 
            // txt_book_name
            // 
            this.txt_book_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_book_name.Location = new System.Drawing.Point(99, 254);
            this.txt_book_name.Name = "txt_book_name";
            this.txt_book_name.Size = new System.Drawing.Size(100, 22);
            this.txt_book_name.TabIndex = 1;
            // 
            // dgvBooks
            // 
            this.dgvBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.asd});
            this.dgvBooks.Location = new System.Drawing.Point(6, 6);
            this.dgvBooks.Name = "dgvBooks";
            this.dgvBooks.RowHeadersWidth = 51;
            this.dgvBooks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBooks.Size = new System.Drawing.Size(687, 225);
            this.dgvBooks.TabIndex = 0;
            this.dgvBooks.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBooks_CellClick);
            // 
            // asd
            // 
            this.asd.DataPropertyName = "images";
            this.asd.HeaderText = "images";
            this.asd.Name = "asd";
            this.asd.Visible = false;
            // 
            // tabPageMagazines
            // 
            this.tabPageMagazines.Controls.Add(this.picMag);
            this.tabPageMagazines.Controls.Add(this.combo_mag_type);
            this.tabPageMagazines.Controls.Add(this.lbl_mag_type);
            this.tabPageMagazines.Controls.Add(this.lbl_mag_sold);
            this.tabPageMagazines.Controls.Add(this.lbl_mag_stock);
            this.tabPageMagazines.Controls.Add(this.lbl_mag_price);
            this.tabPageMagazines.Controls.Add(this.lbl_mag_issue);
            this.tabPageMagazines.Controls.Add(this.lbl_mag_name);
            this.tabPageMagazines.Controls.Add(this.txt_mag_sold);
            this.tabPageMagazines.Controls.Add(this.txt_mag_stock);
            this.tabPageMagazines.Controls.Add(this.txt_mag_price);
            this.tabPageMagazines.Controls.Add(this.txt_mag_issue);
            this.tabPageMagazines.Controls.Add(this.txt_mag_name);
            this.tabPageMagazines.Controls.Add(this.dgvMagazines);
            this.tabPageMagazines.Location = new System.Drawing.Point(4, 22);
            this.tabPageMagazines.Name = "tabPageMagazines";
            this.tabPageMagazines.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMagazines.Size = new System.Drawing.Size(972, 419);
            this.tabPageMagazines.TabIndex = 2;
            this.tabPageMagazines.Text = "Magazines";
            this.tabPageMagazines.UseVisualStyleBackColor = true;
            this.tabPageMagazines.Click += new System.EventHandler(this.tabPageMagazines_Click);
            // 
            // picMag
            // 
            this.picMag.Location = new System.Drawing.Point(705, 6);
            this.picMag.Name = "picMag";
            this.picMag.Size = new System.Drawing.Size(261, 239);
            this.picMag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMag.TabIndex = 4;
            this.picMag.TabStop = false;
            // 
            // combo_mag_type
            // 
            this.combo_mag_type.FormattingEnabled = true;
            this.combo_mag_type.Items.AddRange(new object[] {
            "History",
            "Science",
            "Sport",
            "Computer",
            "Humour",
            "Cook",
            "Economy"});
            this.combo_mag_type.Location = new System.Drawing.Point(157, 334);
            this.combo_mag_type.Name = "combo_mag_type";
            this.combo_mag_type.Size = new System.Drawing.Size(99, 21);
            this.combo_mag_type.TabIndex = 3;
            // 
            // lbl_mag_type
            // 
            this.lbl_mag_type.AutoSize = true;
            this.lbl_mag_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_mag_type.Location = new System.Drawing.Point(87, 339);
            this.lbl_mag_type.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_mag_type.Name = "lbl_mag_type";
            this.lbl_mag_type.Size = new System.Drawing.Size(43, 16);
            this.lbl_mag_type.TabIndex = 2;
            this.lbl_mag_type.Text = "Type:";
            // 
            // lbl_mag_sold
            // 
            this.lbl_mag_sold.AutoSize = true;
            this.lbl_mag_sold.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_mag_sold.Location = new System.Drawing.Point(303, 333);
            this.lbl_mag_sold.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_mag_sold.Name = "lbl_mag_sold";
            this.lbl_mag_sold.Size = new System.Drawing.Size(39, 16);
            this.lbl_mag_sold.TabIndex = 2;
            this.lbl_mag_sold.Text = "Sold:";
            // 
            // lbl_mag_stock
            // 
            this.lbl_mag_stock.AutoSize = true;
            this.lbl_mag_stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_mag_stock.Location = new System.Drawing.Point(303, 307);
            this.lbl_mag_stock.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_mag_stock.Name = "lbl_mag_stock";
            this.lbl_mag_stock.Size = new System.Drawing.Size(45, 16);
            this.lbl_mag_stock.TabIndex = 2;
            this.lbl_mag_stock.Text = "Stock:";
            // 
            // lbl_mag_price
            // 
            this.lbl_mag_price.AutoSize = true;
            this.lbl_mag_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_mag_price.Location = new System.Drawing.Point(303, 284);
            this.lbl_mag_price.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_mag_price.Name = "lbl_mag_price";
            this.lbl_mag_price.Size = new System.Drawing.Size(42, 16);
            this.lbl_mag_price.TabIndex = 2;
            this.lbl_mag_price.Text = "Price:";
            // 
            // lbl_mag_issue
            // 
            this.lbl_mag_issue.AutoSize = true;
            this.lbl_mag_issue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_mag_issue.Location = new System.Drawing.Point(87, 310);
            this.lbl_mag_issue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_mag_issue.Name = "lbl_mag_issue";
            this.lbl_mag_issue.Size = new System.Drawing.Size(43, 16);
            this.lbl_mag_issue.TabIndex = 2;
            this.lbl_mag_issue.Text = "Issue:";
            // 
            // lbl_mag_name
            // 
            this.lbl_mag_name.AutoSize = true;
            this.lbl_mag_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_mag_name.Location = new System.Drawing.Point(87, 284);
            this.lbl_mag_name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_mag_name.Name = "lbl_mag_name";
            this.lbl_mag_name.Size = new System.Drawing.Size(48, 16);
            this.lbl_mag_name.TabIndex = 2;
            this.lbl_mag_name.Text = "Name:";
            // 
            // txt_mag_sold
            // 
            this.txt_mag_sold.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_mag_sold.Location = new System.Drawing.Point(349, 330);
            this.txt_mag_sold.Margin = new System.Windows.Forms.Padding(2);
            this.txt_mag_sold.Name = "txt_mag_sold";
            this.txt_mag_sold.Size = new System.Drawing.Size(99, 22);
            this.txt_mag_sold.TabIndex = 1;
            // 
            // txt_mag_stock
            // 
            this.txt_mag_stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_mag_stock.Location = new System.Drawing.Point(349, 304);
            this.txt_mag_stock.Margin = new System.Windows.Forms.Padding(2);
            this.txt_mag_stock.Name = "txt_mag_stock";
            this.txt_mag_stock.Size = new System.Drawing.Size(99, 22);
            this.txt_mag_stock.TabIndex = 1;
            // 
            // txt_mag_price
            // 
            this.txt_mag_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_mag_price.Location = new System.Drawing.Point(349, 281);
            this.txt_mag_price.Margin = new System.Windows.Forms.Padding(2);
            this.txt_mag_price.Name = "txt_mag_price";
            this.txt_mag_price.Size = new System.Drawing.Size(99, 22);
            this.txt_mag_price.TabIndex = 1;
            // 
            // txt_mag_issue
            // 
            this.txt_mag_issue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_mag_issue.Location = new System.Drawing.Point(157, 307);
            this.txt_mag_issue.Margin = new System.Windows.Forms.Padding(2);
            this.txt_mag_issue.Name = "txt_mag_issue";
            this.txt_mag_issue.Size = new System.Drawing.Size(99, 22);
            this.txt_mag_issue.TabIndex = 1;
            // 
            // txt_mag_name
            // 
            this.txt_mag_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_mag_name.Location = new System.Drawing.Point(157, 281);
            this.txt_mag_name.Margin = new System.Windows.Forms.Padding(2);
            this.txt_mag_name.Name = "txt_mag_name";
            this.txt_mag_name.Size = new System.Drawing.Size(99, 22);
            this.txt_mag_name.TabIndex = 1;
            // 
            // dgvMagazines
            // 
            this.dgvMagazines.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMagazines.Location = new System.Drawing.Point(5, 6);
            this.dgvMagazines.Margin = new System.Windows.Forms.Padding(2);
            this.dgvMagazines.Name = "dgvMagazines";
            this.dgvMagazines.RowHeadersWidth = 51;
            this.dgvMagazines.RowTemplate.Height = 24;
            this.dgvMagazines.Size = new System.Drawing.Size(695, 239);
            this.dgvMagazines.TabIndex = 0;
            this.dgvMagazines.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMagazines_CellClick);
            // 
            // tabPageMusicCD
            // 
            this.tabPageMusicCD.Controls.Add(this.picMusic);
            this.tabPageMusicCD.Controls.Add(this.combo_music_type);
            this.tabPageMusicCD.Controls.Add(this.txt_music_sold);
            this.tabPageMusicCD.Controls.Add(this.lbl_music_sold);
            this.tabPageMusicCD.Controls.Add(this.txt_music_stock);
            this.tabPageMusicCD.Controls.Add(this.lbl_music_stock);
            this.tabPageMusicCD.Controls.Add(this.txt_music_price);
            this.tabPageMusicCD.Controls.Add(this.lbl_music_price);
            this.tabPageMusicCD.Controls.Add(this.txt_music_singer);
            this.tabPageMusicCD.Controls.Add(this.lbl_music_type);
            this.tabPageMusicCD.Controls.Add(this.lbl_music_singer);
            this.tabPageMusicCD.Controls.Add(this.txt_music_name);
            this.tabPageMusicCD.Controls.Add(this.lbl_music_name);
            this.tabPageMusicCD.Controls.Add(this.dgvMusicCD);
            this.tabPageMusicCD.Location = new System.Drawing.Point(4, 22);
            this.tabPageMusicCD.Name = "tabPageMusicCD";
            this.tabPageMusicCD.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMusicCD.Size = new System.Drawing.Size(972, 419);
            this.tabPageMusicCD.TabIndex = 3;
            this.tabPageMusicCD.Text = "MusicCD";
            this.tabPageMusicCD.UseVisualStyleBackColor = true;
            this.tabPageMusicCD.Click += new System.EventHandler(this.tabPageMusicCD_Click);
            // 
            // picMusic
            // 
            this.picMusic.Location = new System.Drawing.Point(701, 6);
            this.picMusic.Name = "picMusic";
            this.picMusic.Size = new System.Drawing.Size(265, 234);
            this.picMusic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMusic.TabIndex = 4;
            this.picMusic.TabStop = false;
            // 
            // combo_music_type
            // 
            this.combo_music_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.combo_music_type.FormattingEnabled = true;
            this.combo_music_type.Items.AddRange(new object[] {
            "Deephouse",
            "Trance",
            "Jazz",
            "Rap",
            "Rock",
            "Türk Sanat Müziği",
            "Arabesque",
            "Reggae"});
            this.combo_music_type.Location = new System.Drawing.Point(130, 333);
            this.combo_music_type.Name = "combo_music_type";
            this.combo_music_type.Size = new System.Drawing.Size(121, 24);
            this.combo_music_type.TabIndex = 3;
            // 
            // txt_music_sold
            // 
            this.txt_music_sold.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_music_sold.Location = new System.Drawing.Point(315, 321);
            this.txt_music_sold.Name = "txt_music_sold";
            this.txt_music_sold.Size = new System.Drawing.Size(100, 22);
            this.txt_music_sold.TabIndex = 2;
            // 
            // lbl_music_sold
            // 
            this.lbl_music_sold.AutoSize = true;
            this.lbl_music_sold.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_music_sold.Location = new System.Drawing.Point(264, 324);
            this.lbl_music_sold.Name = "lbl_music_sold";
            this.lbl_music_sold.Size = new System.Drawing.Size(39, 16);
            this.lbl_music_sold.TabIndex = 1;
            this.lbl_music_sold.Text = "Sold:";
            // 
            // txt_music_stock
            // 
            this.txt_music_stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_music_stock.Location = new System.Drawing.Point(315, 293);
            this.txt_music_stock.Name = "txt_music_stock";
            this.txt_music_stock.Size = new System.Drawing.Size(100, 22);
            this.txt_music_stock.TabIndex = 2;
            // 
            // lbl_music_stock
            // 
            this.lbl_music_stock.AutoSize = true;
            this.lbl_music_stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_music_stock.Location = new System.Drawing.Point(264, 296);
            this.lbl_music_stock.Name = "lbl_music_stock";
            this.lbl_music_stock.Size = new System.Drawing.Size(45, 16);
            this.lbl_music_stock.TabIndex = 1;
            this.lbl_music_stock.Text = "Stock:";
            // 
            // txt_music_price
            // 
            this.txt_music_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_music_price.Location = new System.Drawing.Point(315, 268);
            this.txt_music_price.Name = "txt_music_price";
            this.txt_music_price.Size = new System.Drawing.Size(100, 22);
            this.txt_music_price.TabIndex = 2;
            // 
            // lbl_music_price
            // 
            this.lbl_music_price.AutoSize = true;
            this.lbl_music_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_music_price.Location = new System.Drawing.Point(264, 271);
            this.lbl_music_price.Name = "lbl_music_price";
            this.lbl_music_price.Size = new System.Drawing.Size(42, 16);
            this.lbl_music_price.TabIndex = 1;
            this.lbl_music_price.Text = "Price:";
            // 
            // txt_music_singer
            // 
            this.txt_music_singer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_music_singer.Location = new System.Drawing.Point(130, 296);
            this.txt_music_singer.Name = "txt_music_singer";
            this.txt_music_singer.Size = new System.Drawing.Size(100, 22);
            this.txt_music_singer.TabIndex = 2;
            // 
            // lbl_music_type
            // 
            this.lbl_music_type.AutoSize = true;
            this.lbl_music_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_music_type.Location = new System.Drawing.Point(79, 333);
            this.lbl_music_type.Name = "lbl_music_type";
            this.lbl_music_type.Size = new System.Drawing.Size(43, 16);
            this.lbl_music_type.TabIndex = 1;
            this.lbl_music_type.Text = "Type:";
            // 
            // lbl_music_singer
            // 
            this.lbl_music_singer.AutoSize = true;
            this.lbl_music_singer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_music_singer.Location = new System.Drawing.Point(79, 299);
            this.lbl_music_singer.Name = "lbl_music_singer";
            this.lbl_music_singer.Size = new System.Drawing.Size(50, 16);
            this.lbl_music_singer.TabIndex = 1;
            this.lbl_music_singer.Text = "Singer:";
            // 
            // txt_music_name
            // 
            this.txt_music_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_music_name.Location = new System.Drawing.Point(130, 268);
            this.txt_music_name.Name = "txt_music_name";
            this.txt_music_name.Size = new System.Drawing.Size(100, 22);
            this.txt_music_name.TabIndex = 2;
            // 
            // lbl_music_name
            // 
            this.lbl_music_name.AutoSize = true;
            this.lbl_music_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_music_name.Location = new System.Drawing.Point(79, 271);
            this.lbl_music_name.Name = "lbl_music_name";
            this.lbl_music_name.Size = new System.Drawing.Size(48, 16);
            this.lbl_music_name.TabIndex = 1;
            this.lbl_music_name.Text = "Name:";
            // 
            // dgvMusicCD
            // 
            this.dgvMusicCD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMusicCD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.images});
            this.dgvMusicCD.Location = new System.Drawing.Point(6, 6);
            this.dgvMusicCD.Name = "dgvMusicCD";
            this.dgvMusicCD.Size = new System.Drawing.Size(689, 234);
            this.dgvMusicCD.TabIndex = 0;
            this.dgvMusicCD.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMusicCD_CellClick);
            // 
            // images
            // 
            this.images.DataPropertyName = "images";
            this.images.HeaderText = "images";
            this.images.Name = "images";
            this.images.Visible = false;
            // 
            // btnShow
            // 
            this.btnShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnShow.Location = new System.Drawing.Point(217, 499);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(96, 29);
            this.btnShow.TabIndex = 1;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAdd.Location = new System.Drawing.Point(319, 499);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(88, 29);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDelete.Location = new System.Drawing.Point(413, 499);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(88, 29);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUpdate.Location = new System.Drawing.Point(507, 499);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(88, 29);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnChoosePic
            // 
            this.btnChoosePic.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnChoosePic.Location = new System.Drawing.Point(715, 495);
            this.btnChoosePic.Name = "btnChoosePic";
            this.btnChoosePic.Size = new System.Drawing.Size(122, 29);
            this.btnChoosePic.TabIndex = 5;
            this.btnChoosePic.Text = "Choose Picture";
            this.btnChoosePic.UseVisualStyleBackColor = true;
            this.btnChoosePic.Click += new System.EventHandler(this.btnChoosePic_Click);
            // 
            // frmAdminPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 590);
            this.Controls.Add(this.btnChoosePic);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblAdminPanel);
            this.Controls.Add(this.btnShow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAdminPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAdminPanel";
            this.tabControl1.ResumeLayout(false);
            this.tabPageUsers.ResumeLayout(false);
            this.tabPageUsers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsers)).EndInit();
            this.tabPageBooks.ResumeLayout(false);
            this.tabPageBooks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBooks)).EndInit();
            this.tabPageMagazines.ResumeLayout(false);
            this.tabPageMagazines.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMagazines)).EndInit();
            this.tabPageMusicCD.ResumeLayout(false);
            this.tabPageMusicCD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMusic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMusicCD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAdminPanel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageUsers;
        private System.Windows.Forms.TabPage tabPageBooks;
        private System.Windows.Forms.TabPage tabPageMagazines;
        private System.Windows.Forms.TabPage tabPageMusicCD;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.DataGridView dgvUsers;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnChoosePic;
        private System.Windows.Forms.DataGridView dgvBooks;
        private System.Windows.Forms.Label lbl_book_price;
        private System.Windows.Forms.Label lbl_book_page;
        private System.Windows.Forms.Label lbl_book_ISBN;
        private System.Windows.Forms.Label lbl_book_publisher;
        private System.Windows.Forms.Label lbl_book_author;
        private System.Windows.Forms.Label lbl_book_name;
        private System.Windows.Forms.TextBox txt_book_price;
        private System.Windows.Forms.TextBox txt_book_page;
        private System.Windows.Forms.TextBox txt_book_ISBN;
        private System.Windows.Forms.TextBox txt_book_publisher;
        private System.Windows.Forms.TextBox txt_book_author;
        private System.Windows.Forms.TextBox txt_book_name;
        private System.Windows.Forms.Label lbl_u_phone;
        private System.Windows.Forms.TextBox txt_u_username;
        private System.Windows.Forms.Label lbl_u_username;
        private System.Windows.Forms.TextBox txt_u_address;
        private System.Windows.Forms.Label lbl_u_address;
        private System.Windows.Forms.TextBox txt_u_surname;
        private System.Windows.Forms.Label lbl_u_surname;
        private System.Windows.Forms.TextBox txt_u_name;
        private System.Windows.Forms.Label lbl_u_name;
        private System.Windows.Forms.Label lbl_mag_name;
        private System.Windows.Forms.TextBox txt_mag_name;
        private System.Windows.Forms.DataGridView dgvMagazines;
        private System.Windows.Forms.Label lbl_mag_type;
        private System.Windows.Forms.Label lbl_mag_price;
        private System.Windows.Forms.Label lbl_mag_issue;
        private System.Windows.Forms.TextBox txt_mag_price;
        private System.Windows.Forms.TextBox txt_mag_issue;
        private System.Windows.Forms.ComboBox combo_mag_type;
        private System.Windows.Forms.ComboBox combo_music_type;
        private System.Windows.Forms.TextBox txt_music_price;
        private System.Windows.Forms.Label lbl_music_price;
        private System.Windows.Forms.TextBox txt_music_singer;
        private System.Windows.Forms.Label lbl_music_type;
        private System.Windows.Forms.Label lbl_music_singer;
        private System.Windows.Forms.TextBox txt_music_name;
        private System.Windows.Forms.Label lbl_music_name;
        private System.Windows.Forms.DataGridView dgvMusicCD;
        private System.Windows.Forms.TextBox txt_u_password;
        private System.Windows.Forms.Label lbl_u_password;
        private System.Windows.Forms.CheckBox checkBox_isAdmin;
        private System.Windows.Forms.MaskedTextBox masked_txt_phone;
        private System.Windows.Forms.Label lbl_u_attention;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picMag;
        private System.Windows.Forms.PictureBox picMusic;
        private System.Windows.Forms.DataGridViewTextBoxColumn images;
        private System.Windows.Forms.DataGridViewTextBoxColumn asd;
        public System.Windows.Forms.PictureBox picBooks;
        private System.Windows.Forms.Label lbl_book_sold;
        private System.Windows.Forms.Label lbl_book_stock;
        private System.Windows.Forms.TextBox txt_book_sold;
        private System.Windows.Forms.TextBox txt_book_stock;
        private System.Windows.Forms.Label lbl_mag_sold;
        private System.Windows.Forms.Label lbl_mag_stock;
        private System.Windows.Forms.TextBox txt_mag_sold;
        private System.Windows.Forms.TextBox txt_mag_stock;
        private System.Windows.Forms.TextBox txt_music_sold;
        private System.Windows.Forms.Label lbl_music_sold;
        private System.Windows.Forms.TextBox txt_music_stock;
        private System.Windows.Forms.Label lbl_music_stock;
    }
}