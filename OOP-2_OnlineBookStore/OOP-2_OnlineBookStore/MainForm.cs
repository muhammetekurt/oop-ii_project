﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OOP_2_OnlineBookStore
{
    public partial class MainForm : Form
    {
        string connectionString = "Data Source = SQL5097.site4now.net; Initial Catalog = db_a750b9_projeodevi2; User Id = db_a750b9_projeodevi2_admin; Password = admin2626";


        int index = -1;
        int index2 = 0;
        BookProvider bp = new BookProvider();
        List<Book> bListe = new List<Book>();

        MagazineProvider mp = new MagazineProvider();
        List<Magazine> mListe = new List<Magazine>();

        MusicCDProvider cp = new MusicCDProvider();
        List<MusicCD> cdListe = new List<MusicCD>();

        ShoppingCart SK2 = new ShoppingCart();

        LoginedCustomer LC;


        public MainForm()
        {
            InitializeComponent();
      
        }
        private void Dashboard2_Load(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {                
                bListe = bp.booklistFromDatabase();
                dataGridView1.DataSource = bListe.Select(Book => new {Name = Book.name, Price = Book.price }).ToList();
                pictureBox1.Image = ByteToImage(bListe[0].image);
            }
            else if (tabControl1.SelectedIndex == 1)
            {                
                mListe = mp.magazinelistFromDatabase();
                dataGridView1.DataSource = mListe.Select(Magazine => new { Name = Magazine.name, Price = Magazine.price, Issue = Magazine.Issue }).ToList();
                pictureBox2.Image = ByteToImage(mListe[0].image);
            }
            else if (tabControl1.SelectedIndex == 2)
            {
                cdListe = cp.musicCDlistFromDatabase();
                dataGridView1.DataSource = cdListe.Select(MusicCD => new { Name = MusicCD.name, Price = MusicCD.price }).ToList();
                pictureBox3.Image = ByteToImage(cdListe[0].image);
            }
            LC = LoginedCustomer.Singleton();
                       
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {                
                index2 = 0;
                bListe = bp.booklistFromDatabase();
                dataGridView1.DataSource = bListe.Select(Book => new { Name = Book.name, Price = Book.price }).ToList();

            }
            else if (tabControl1.SelectedIndex == 1)
            {               
                index2 = 0;
                mListe = mp.magazinelistFromDatabase();
                dataGridView2.DataSource = mListe.Select(Magazine => new { Name = Magazine.name, Price = Magazine.price, Issue = Magazine.Issue }).ToList();
                pictureBox2.Image = ByteToImage(mListe[0].image);
            }
            else if (tabControl1.SelectedIndex == 2)
            {
                index2 = 0;
                cdListe = cp.musicCDlistFromDatabase();
                pictureBox3.Image = ByteToImage(cdListe[0].image);

                dataGridView3.DataSource = cdListe.Select(MusicCD => new { Name = MusicCD.name, Price = MusicCD.price }).ToList(); 
                
            }
        }
        
        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
           
            if(pData!=null)
            {
                try
                {
                    mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                    Bitmap bm = new Bitmap(mStream, false);
                    mStream.Dispose();
                    return bm;
                }
                catch
                {                  
                }
            }
            MessageBox.Show("This product does not have an image to display!");
            return null;
            
        }

        
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
               
                bListe = bp.booklistFromDatabase();
                index = e.RowIndex;
                pictureBox1.Image = ByteToImage(bListe[index].image);                
                index2 = index;
            }
        }
        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {                
                mListe = mp.magazinelistFromDatabase();
                index = e.RowIndex;                
                pictureBox2.Image = ByteToImage(mListe[index].image);                
                index2 = index;
            }
        }
        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {               
                                
                cdListe = cp.musicCDlistFromDatabase();
                index = e.RowIndex;                
                
                pictureBox3.Image = ByteToImage(cdListe[index].image);                
                index2 = index;

            }
        }

        List<ItemToPurchase> basket = new List<ItemToPurchase>();
                       
                
        private void button1_Click(object sender, EventArgs e)
        {
            ItemToPurchase item = new ItemToPurchase();

            if (tabControl1.SelectedIndex == 0)
            {
                item.Product = bListe[index2];
               
            }
            else if (tabControl1.SelectedIndex == 1)
            {
                item.Product = mListe[index2];
                
            }
            if (tabControl1.SelectedIndex == 2)
            {
                item.Product = cdListe[index2];
                
            }

            item.Quantity = 1;

            SK2.ItemsToPurchase.Add(item);
           
            DataTable dt = new DataTable();
            dt.Clear();
            DataColumn dtcol = new DataColumn("Name");
            DataColumn dtcol1 = new DataColumn("Price");
            DataColumn dtcol2 = new DataColumn("Quantity");
            dt.Columns.Add(dtcol);
            dt.Columns.Add(dtcol1);
            dt.Columns.Add(dtcol2);
     
            for (int i = 0; i < SK2.ItemsToPurchase.Count; i++)
            {                
                dt.Rows.Add(SK2.ItemsToPurchase[i].Product.name, SK2.ItemsToPurchase[i].Product.price, 1);
            }
            
            dgvBasket.DataSource = dt;
            dgvBasket.Refresh();
                   

        }
            
        private void btnRemoveFromBasket_Click(object sender, EventArgs e)
        {
            int Index = dgvBasket.CurrentCell.RowIndex;
            SK2.ItemsToPurchase.RemoveAt(Index);

            DataTable dt = new DataTable();
            //dt.Clear();

            DataColumn dtcol = new DataColumn("Name");
            DataColumn dtcol1 = new DataColumn("Price");
            DataColumn dtcol2 = new DataColumn("Quantity");
            dt.Columns.Add(dtcol);
            dt.Columns.Add(dtcol1);
            dt.Columns.Add(dtcol2);
            for (int i = 0; i < SK2.ItemsToPurchase.Count; i++)
            {
                dt.Rows.Add(SK2.ItemsToPurchase[i].Product.name, SK2.ItemsToPurchase[i].Product.price, 1);
                
            }
                      

            dgvBasket.DataSource = dt;
            dgvBasket.Refresh();
            
        }
              
        private void btnClearBasket_Click(object sender, EventArgs e)
        {


            SK2.ItemsToPurchase.Clear();
            DataTable dt = new DataTable();
         
            DataColumn dtcol = new DataColumn("Name");
            DataColumn dtcol1 = new DataColumn("Price");
            DataColumn dtcol2 = new DataColumn("Quantity");
            dt.Columns.Add(dtcol);
            dt.Columns.Add(dtcol1);
            dt.Columns.Add(dtcol2);
            for (int i = 0; i < SK2.ItemsToPurchase.Count; i++)
            {
                dt.Rows.Add(SK2.ItemsToPurchase[i].Product.name, SK2.ItemsToPurchase[i].Product.price, 1);
            }

            

            dgvBasket.DataSource = dt;
            dgvBasket.Refresh();
            
        }
        bool durum;
        public void ControlRepetition(string column, string table, string a)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlCommand cmd = new SqlCommand("Select " + column + " from " + table + " where " + column + "= '" + a + "'", sqlCon);
            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read())
            {
                durum = false;
            }
            else
            {
                durum = true;
            }

            sqlCon.Close();

        }


        public int QuantityCalculator(string a)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlCommand cmd = new SqlCommand("Select quantity from Siparis where name='"+a+"'", sqlCon);
            int result = Convert.ToInt32(cmd.ExecuteScalar());
            sqlCon.Close();
            return result;

        }
        private void btnPayment_Click(object sender, EventArgs e)
        {
            
            for (int i = 0; i < SK2.ItemsToPurchase.Count; i++)
            {
                
                ControlRepetition("name", "Siparis", SK2.ItemsToPurchase[i].Product.name);

                if(durum==true)
                {
                    SqlConnection sqlCon = new SqlConnection(connectionString);
                    sqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("SiparisOlustur", sqlCon);                    
                    sqlCmd.CommandType = CommandType.StoredProcedure;                   
                    LoginedCustomer LC = LoginedCustomer.Singleton();
                    sqlCmd.Parameters.AddWithValue("@user_id",Convert.ToInt32(LC.User.getCcustomerID()));
                    sqlCmd.Parameters.AddWithValue("@name", SK2.ItemsToPurchase[i].Product.name);
                    sqlCmd.Parameters.AddWithValue("@price", SK2.ItemsToPurchase[i].Product.price);
                    sqlCmd.Parameters.AddWithValue("@quantity", 1);
                    sqlCmd.ExecuteNonQuery();
                    sqlCmd.Parameters.Clear();                    
                    sqlCon.Close();

                }

                else
                {
                    int adet = QuantityCalculator(SK2.ItemsToPurchase[i].Product.name);
                    adet++;
                    SqlConnection sqlCon = new SqlConnection(connectionString);
                    sqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("UPDATE Siparis SET quantity='" + adet + "'Where name='"+SK2.ItemsToPurchase[i].Product.name+"'", sqlCon);
                                       
                    sqlCmd.ExecuteNonQuery();
                    sqlCmd.Parameters.Clear();                    
                    sqlCon.Close();
                }
               
            }

            Payment payment = new Payment();
            this.Hide();
            payment.Show();

        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            frmLogin frm = new frmLogin();
            frm.Show();
            this.Hide();
           
        }
    }
}
