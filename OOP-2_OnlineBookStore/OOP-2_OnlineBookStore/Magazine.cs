﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_2_OnlineBookStore
{ 
    class Magazine : Product
    {
        private int issue;
        private string type;
        /**
        * Parametresiz constructor
        */
        public Magazine() { }
        /**
        * Parametreli constructor
        */
        public Magazine(int issue, string type, string name, decimal price, byte[] image, int stock, int soldCount, int id)
        {
            this.issue = issue;
            this.type = type;
            this.name = name;
            this.id = id;
            this.price = price;
            this.image = image;
            this.stock = stock;
            this.soldCount = soldCount;
        }

        public string Type { get => type; set => type = value; }
        public int Issue { get => issue; set => issue = value; }
        /**
        * Urunun ismini dondurur
        * @return name
        */
        public override string getProductName()
        {
            return this.name;
        }
        /**
        * Urunun idsini dondurur
        * @return id
        */
        public override int getProductID()
        {
            return this.id;
        }
        /**
        * Urunun fiyatini dondurur
        * @return price
        */
        public override decimal getProductPrice()
        {
            return this.price;
        }

        /**
        * Bu fonksiyon customer nesnesinin ozelliklerini yazdirir
        */
        public override string printProperties()
        {
            return "Issue: " + this.issue + "\nType: " + this.type;
        }
    }
}
