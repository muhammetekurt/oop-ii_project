﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Drawing.Imaging;  

namespace OOP_2_OnlineBookStore
{
    public partial class frmAdminPanel : Form
    {
        string connectionString = "Data Source = SQL5097.site4now.net; Initial Catalog = db_a750b9_projeodevi2; User Id = db_a750b9_projeodevi2_admin; Password = admin2626";
        int selectedRowIndex;string imagepath; 
        string selectedRowUsername; int selectedRowBooks ; int selectedRowMagazines; int selectedRowMusicCD;
        

        public frmAdminPanel()
        {
            InitializeComponent();

        }
       
        
        private void btnShow_Click(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);

            if (tabControl1.SelectedIndex == 0)
            {
                ShowTablesData("Select * from Users", dgvUsers);
            }
            else if (tabControl1.SelectedIndex == 1)
            {
                ShowTablesData("Select id,name, author, publisher, price, ISBN,page, images,stock, soldCount from Product where product_type=1 ", dgvBooks);

            }
            else if (tabControl1.SelectedIndex == 2)
            {
                ShowTablesData("Select id,name, issue, type, price, images,stock,soldCount from Product where product_type=2 ", dgvMagazines);
            }
            else if (tabControl1.SelectedIndex == 3)
            {
                ShowTablesData("Select id,name, singer, type, price, images,stock,soldCount from Product where product_type=3 ", dgvMusicCD);
            }
        }

        public void ShowTablesData(string query, DataGridView dgv)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            SqlDataAdapter sda = new SqlDataAdapter(query, sqlCon);
            DataTable datatable = new DataTable();
            sda.Fill(datatable);
            dgv.DataSource = datatable;
            sqlCon.Close();
        }

        private void btnChoosePic_Click(object sender, EventArgs e)
        {

            OpenFileDialog openfiledialog = new OpenFileDialog();
            openfiledialog.Title = "Choose a picture";
            openfiledialog.Filter = "Image Files(*.jpg; *.jpeg; *.png)|*.jpg; *.jpeg; *.png";
            if (openfiledialog.ShowDialog() == DialogResult.OK)
            {   
                int a= tabControl1.SelectedIndex;   
                switch (a)
                {
                    case (1):
                        picBooks.Image = Image.FromFile(openfiledialog.FileName);
                        imagepath = openfiledialog.FileName;
                        break;
                    case (2):
                        picMag.Image = Image.FromFile(openfiledialog.FileName);
                        imagepath = openfiledialog.FileName;
                        break;
                    case (3):
                        picMusic.Image = Image.FromFile(openfiledialog.FileName);
                        imagepath = openfiledialog.FileName;
                        break;
                    default:
                        break;
                }
            }

        }
       
        public byte[] ResimOkuma(string imagepath)
        {
            FileStream fileStream = new FileStream(imagepath, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);
            byte[] picture = binaryReader.ReadBytes((int)fileStream.Length);
            binaryReader.Close();
            fileStream.Close();
            return picture;
        }
                
        private void tabPageBooks_Click(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            string show_query = "Select id,name, author, publisher, price, ISBN,page, images,stock, soldCount from Product where product_type=1";
            SqlDataAdapter sda = new SqlDataAdapter(show_query, sqlCon);
            DataTable datatable = new DataTable();
            sda.Fill(datatable);
            dgvBooks.DataSource = datatable;
            sqlCon.Close();

        }
        
        private void tabPageMagazines_Click(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            string show_query = "Select id, name, issue, type, price,images, stock, soldCount from Product where product_type=2";
            SqlDataAdapter sda = new SqlDataAdapter(show_query, sqlCon);
            DataTable datatable = new DataTable();
            sda.Fill(datatable);
            dgvMagazines.DataSource = datatable;

          
            sqlCon.Close();
        }
       
        private void tabPageMusicCD_Click(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            string show_query = "Select id,name, singer, type, price, images, stock, soldCount from Product where product_type=3";
            SqlDataAdapter sda = new SqlDataAdapter(show_query, sqlCon);
            DataTable datatable = new DataTable();
            sda.Fill(datatable);
            dgvMusicCD.DataSource = datatable;
            sqlCon.Close();
        }

        bool durum;
        public void ControlRepetition(string column, string table, TextBox tb)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlCommand cmd = new SqlCommand("Select " + column + " from " + table + " where " + column + "= '" + tb.Text + "'", sqlCon);
            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read())
            {
                durum = false;
            }
            else
            {
                durum = true;
            }

            sqlCon.Close();

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0) 
            {
                if (txt_u_name.Text == "" || txt_u_surname.Text == "" || masked_txt_phone.Text == "" || txt_u_address.Text == ""
                     || txt_u_username.Text == "" || txt_u_password.Text == "")
                {
                    MessageBox.Show("Boş alan bırakamazsınız...", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    ControlRepetition("user_username", "Users", txt_u_username);
                    if (durum == true)
                    {
                        SqlConnection sqlCon = new SqlConnection(connectionString);
                        sqlCon.Open();
                        SqlCommand sqlCmd = new SqlCommand("UserAdd", sqlCon);
                        MessageBox.Show(sqlCon.State.ToString());            
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@user_name", txt_u_name.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@user_surname", txt_u_surname.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@user_username", txt_u_username.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@user_password", txt_u_password.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@user_phone", masked_txt_phone.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@user_address", txt_u_address.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@isAdmin", checkBox_isAdmin.ThreeState);
                        sqlCmd.ExecuteNonQuery();
                        MessageBox.Show("Registration Succesfully");
                        sqlCon.Close();
                    }
                    else
                    {

                        MessageBox.Show("Bu isim zaten var...", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }


            }
            else if (tabControl1.SelectedIndex == 1) 
            {

                if (txt_book_name.Text == "" || txt_book_price.Text == "" || txt_book_ISBN.Text == "" || txt_book_author.Text == ""
                    || txt_book_publisher.Text == "" || txt_book_page.Text == "")
                {
                    MessageBox.Show("Boş alan bırakamazsınız...", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    ControlRepetition("name", "Product", txt_book_name);
                    if (durum == true)
                    {
                        SqlConnection sqlCon = new SqlConnection(connectionString);
                        sqlCon.Open();
                        SqlCommand sqlCmd = new SqlCommand("BookAdd", sqlCon);
                        MessageBox.Show(sqlCon.State.ToString());            
                       
                        byte[] pic1=ResimOkuma(imagepath);
                        
                        sqlCmd.CommandType = CommandType.StoredProcedure;

                        sqlCmd.Parameters.AddWithValue("@name", txt_book_name.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@price", Convert.ToInt32(txt_book_price.Text));
                        sqlCmd.Parameters.AddWithValue("@product_type", 1);                                
                        sqlCmd.Parameters.AddWithValue("@ISBN", Convert.ToDecimal(txt_book_ISBN.Text));
                        sqlCmd.Parameters.AddWithValue("@author", txt_book_author.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@publisher", txt_book_publisher.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@page", Convert.ToInt32(txt_book_page.Text));
                        sqlCmd.Parameters.Add("@images", SqlDbType.Image, pic1.Length).Value = pic1;   
                        sqlCmd.Parameters.AddWithValue("@stock", Convert.ToInt32(txt_book_stock.Text));
                        sqlCmd.Parameters.AddWithValue("@soldCount", Convert.ToInt32(txt_book_sold.Text));

                        sqlCmd.ExecuteNonQuery();
                        sqlCon.Close();
                        
                        MessageBox.Show("Registration Succesfully");
                        

                    }
                    else
                    {
                        MessageBox.Show("Bu isimde bir kitap zaten var...", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    }

                }

            }
            else if (tabControl1.SelectedIndex == 2) 
            {
                if (txt_mag_name.Text == "" || txt_mag_price.Text == "" || txt_mag_issue.Text == "" || combo_mag_type.Text == "")
                {
                    MessageBox.Show("Boş alan bırakamazsınız...", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {                    
                        SqlConnection sqlCon = new SqlConnection(connectionString);
                        sqlCon.Open();
                        SqlCommand sqlCmd = new SqlCommand("MagazineAdd", sqlCon);
                        MessageBox.Show(sqlCon.State.ToString());            

                        byte[] pic1 = ResimOkuma(imagepath);

                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@name", txt_mag_name.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@price", Convert.ToInt32(txt_mag_price.Text));
                        sqlCmd.Parameters.AddWithValue("@product_type", 2);                                
                        sqlCmd.Parameters.AddWithValue("@type", combo_mag_type.GetItemText(combo_mag_type.SelectedItem));
                        sqlCmd.Parameters.AddWithValue("@issue", Convert.ToInt32(txt_mag_issue.Text));
                        sqlCmd.Parameters.Add("@images", SqlDbType.Image, pic1.Length).Value = pic1;   
                        sqlCmd.Parameters.AddWithValue("@stock", Convert.ToInt32(txt_mag_stock.Text));
                        sqlCmd.Parameters.AddWithValue("@soldCount", Convert.ToInt32(txt_mag_sold.Text));
                        sqlCmd.ExecuteNonQuery();
                        MessageBox.Show(String.Format("Magazine {0} ready for sale! ", txt_mag_name.Text));
                        sqlCon.Close();                   

                }

            }
            else if (tabControl1.SelectedIndex == 3) 
            {
                if (txt_music_name.Text == "" || txt_music_price.Text == "" || txt_music_singer.Text == "" || combo_music_type.Text == "")
                {
                    MessageBox.Show("Boş alan bırakamazsınız...", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    
                        SqlConnection sqlCon = new SqlConnection(connectionString);
                        sqlCon.Open();
                        SqlCommand sqlCmd = new SqlCommand("MusicCDAdd", sqlCon);
                        MessageBox.Show(sqlCon.State.ToString());            

                        byte[] pic1 = ResimOkuma(imagepath);

                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@name", txt_music_name.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@price", Convert.ToInt32(txt_music_price.Text));
                        sqlCmd.Parameters.AddWithValue("@product_type", 3);                                
                        sqlCmd.Parameters.AddWithValue("@singer", txt_music_singer.Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@type", combo_music_type.GetItemText(combo_music_type.SelectedItem));
                        sqlCmd.Parameters.Add("@images", SqlDbType.Image, pic1.Length).Value = pic1;   
                        sqlCmd.ExecuteNonQuery();
                        
                        MessageBox.Show(String.Format("NOW LISTEN! {0} ! ", txt_music_singer.Text));
                        sqlCon.Close();
                   
                }
            }
        }

        public void SQL_Query(string query, DataGridView dgv )
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();            
            SqlDataAdapter sda = new SqlDataAdapter(query, sqlCon);
            DataTable datatable = new DataTable();
            sda.Fill(datatable);
            dgv.DataSource = datatable;
            sqlCon.Close();

        }

        public void DeleteData(string islem)
        {
            string show_query="";             

            switch (islem)
            {
                case "Users":
                    
                    show_query = "delete from Users where user_id=" + dgvUsers.SelectedCells[0].Value;
                    SQL_Query(show_query, dgvUsers);                                       
                    break;
                
                case "Books":
                    show_query = "delete from Product where id=" + dgvBooks.SelectedCells[0].Value;
                    SQL_Query(show_query, dgvBooks);
                    break;

                case "Magazines":
                    show_query = "delete from Product where id=" + dgvMagazines.SelectedCells[0].Value;
                    SQL_Query(show_query, dgvMagazines);
                    break;
                case "MusicCDs":
                    show_query = @"delete from Product where id=" + dgvMusicCD.SelectedCells[0].Value;
                    SQL_Query(show_query, dgvMusicCD);
                    break;
                default:
                    break;
            }
        }
     
        private void btnDelete_Click(object sender, EventArgs e)
        {
           
            if (tabControl1.SelectedIndex == 0) 
            {
                if (dgvUsers.SelectedRows.Count ==0  )
                {                    
                    MessageBox.Show("Lütfen listeden seçim yapınız user", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {                    
                    DeleteData("Users");
                }
            }
            else if (tabControl1.SelectedIndex == 1) 
            {
                if (dgvBooks.SelectedRows.Count ==0)
                {
                    MessageBox.Show("Lütfen listeden seçim yapınız book", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    MessageBox.Show(dgvBooks.SelectedCells[0].Value.ToString());
                }
                else
                {
                    DeleteData("Books");
                }
            }
            else if (tabControl1.SelectedIndex == 2)
            {
                if (dgvMagazines.SelectedRows.Count == 0)
                {
                    MessageBox.Show("Lütfen listeden seçim yapını magazine","Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DeleteData("Magazines");
                }

            }
            else if (tabControl1.SelectedIndex == 3)
            {
                if (dgvMusicCD.SelectedRows.Count == 0)
                {
                    MessageBox.Show("Lütfen listeden seçim yapınız","Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DeleteData("MusicCDs");
                }

            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)             
        {
            if (tabControl1.SelectedIndex == 0) 
            {
                if (txt_u_name.Text == "" || txt_u_surname.Text == "" || masked_txt_phone.Text == "" || txt_u_address.Text == ""
                     || txt_u_username.Text == "" || txt_u_password.Text == "")
                {
                    MessageBox.Show("Boş alan bırakamazsınız...", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    int result = 0;
                    string chechUsername = txt_u_username.Text;
                    SqlConnection sqlCon = new SqlConnection(connectionString);
                    sqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("SELECT COUNT(*) FROM Users WHERE user_username ='"+ chechUsername+"'", sqlCon);
                    result = Convert.ToInt32(sqlCmd.ExecuteScalar());
                    sqlCon.Close();
                    
                    if (result != 0)
                    {
                        MessageBox.Show("Username already exists");
                    }
                    else
                    {
                    sqlCon.Open();                   
                    SqlCommand sqlCmd1 = new SqlCommand("UPDATE Users SET user_name='" + txt_u_name.Text + "',user_surname='" + txt_u_surname.Text + "',user_username='" + txt_u_username.Text + "',user_password='" + txt_u_password.Text + "'," +
                           "user_phone='" + masked_txt_phone.Text + "',user_address='" + txt_u_address.Text + "',isAdmin='" + checkBox_isAdmin.Checked + "' WHERE user_username='" + selectedRowUsername + "'", sqlCon);
                    
                    sqlCmd1.ExecuteNonQuery();
                   
                    MessageBox.Show("Changes Saved for user...","Information",MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    sqlCon.Close();
                    }

                }
            }

            else if (tabControl1.SelectedIndex == 1) 
            {
                if (txt_book_name.Text == "" || txt_book_price.Text == "" || txt_book_ISBN.Text == "" || txt_book_author.Text == ""
                    || txt_book_publisher.Text == "" || txt_book_page.Text == "")
                {
                    MessageBox.Show("Boş alan bırakamazsınız...", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    int result = 0;
                    string chechBookName = txt_book_name.Text;
                    SqlConnection sqlCon = new SqlConnection(connectionString);
                    sqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("SELECT COUNT(*) FROM Product WHERE product_type= 1 and name ='" + chechBookName + "'", sqlCon);
                    result = Convert.ToInt32(sqlCmd.ExecuteScalar());
                    sqlCon.Close();

                        sqlCon.Open();
                        SqlCommand sqlCmd1 = new SqlCommand("UPDATE Product SET name='" + txt_book_name.Text + "',price='" + Convert.ToInt32(txt_book_price.Text) + "',ISBN='" + Convert.ToDecimal(txt_book_ISBN.Text) + "',author='" + txt_book_author.Text + "'," +
                               "publisher='" + txt_book_publisher.Text + "',page='" + Convert.ToInt32(txt_book_page.Text) + "',stock='" + Convert.ToInt32(txt_book_stock.Text) + "',soldCount='" + Convert.ToInt32(txt_book_sold.Text) + "' WHERE id='" + selectedRowBooks + "'", sqlCon);

                        sqlCmd1.ExecuteNonQuery();

                        MessageBox.Show("Changes Saved for book...");

                        sqlCon.Close();
                  

                }
            }


            else if (tabControl1.SelectedIndex == 2) 
            {
                if (txt_mag_name.Text == "" || txt_mag_price.Text == "" || txt_mag_issue.Text == "" || combo_mag_type.Text == "" || txt_mag_stock.Text == "")
                {
                    MessageBox.Show("Boş alan bırakamazsınız...", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {                   
                    string chechMagName = txt_mag_name.Text;
                    SqlConnection sqlCon = new SqlConnection(connectionString);                    
                    sqlCon.Open();
                     SqlCommand sqlCmd1 = new SqlCommand("UPDATE Product SET name='" + txt_mag_name.Text + "',price='" + Convert.ToInt32(txt_mag_price.Text) + "',type='" + combo_mag_type.GetItemText(combo_mag_type.SelectedItem) 
                            + "',issue='" + Convert.ToInt32(txt_mag_issue.Text) + "',stock='" + Convert.ToInt32(txt_mag_stock.Text)+ "',soldCount='" + Convert.ToInt32(txt_mag_sold.Text) + "' WHERE id='" + selectedRowMagazines + "'", sqlCon);
                                               
                    sqlCmd1.ExecuteNonQuery();
                    MessageBox.Show("Changes Saved for magazine...");
                     sqlCon.Close();                   

                }
            }


            else if (tabControl1.SelectedIndex == 3) 
            {
                if (txt_music_name.Text == "" || txt_music_price.Text == "" || txt_music_singer.Text == "" || combo_music_type.Text == "" || txt_music_stock.Text == "")
                {
                    MessageBox.Show("Boş alan bırakamazsınız...", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {                    
                    SqlConnection sqlCon = new SqlConnection(connectionString);

                    sqlCon.Open();
                    SqlCommand sqlCmd1 = new SqlCommand("UPDATE Product SET name='" + txt_music_name.Text + "',price='" + Convert.ToInt32(txt_music_price.Text) + "',type='" + combo_music_type.GetItemText(combo_music_type.SelectedItem)
                           + "',singer='" + txt_music_singer.Text + "',stock='" + txt_music_stock.Text + "',soldCount='" + txt_music_sold.Text + "' WHERE id='" + selectedRowMusicCD + "'", sqlCon);

                    sqlCmd1.ExecuteNonQuery();
                    MessageBox.Show("Changes Saved for Music CD...");
                    sqlCon.Close();

                }
            }

        }

        public  void PictureToPictureBox(DataGridView dgv , PictureBox picBox,int a)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlCommand cmd = new SqlCommand("select images from Product where id='" + a + "'", sqlCon);
            SqlDataReader dr = cmd.ExecuteReader();

            if(dr.Read())
            {
                if (dr["images"] != DBNull.Value)
                {
                    picBox.Visible = true;
                    byte[] picture = new byte[0];
                    picture = (byte[])dr["images"];
                    MemoryStream memoryStream = new MemoryStream(picture);
                    picBox.Image = Image.FromStream(memoryStream);
                    dr.Close();
                    cmd.Dispose();
                    sqlCon.Close();
                }
                else
                {
                    picBox.Visible = false;
                    MessageBox.Show("This product does not have an image...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dr.Close();
                    cmd.Dispose();
                    sqlCon.Close();
                }
            }
            sqlCon.Close();

        }


        private void dgvMusicCD_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex!=-1)
            {
                DataGridViewRow row = dgvMusicCD.Rows[e.RowIndex];
                selectedRowMusicCD= Convert.ToInt32(row.Cells[0].Value);
                txt_music_name.Text = row.Cells[1].Value.ToString();
                txt_music_singer.Text = row.Cells[2].Value.ToString();
                combo_music_type.Text = row.Cells[3].Value.ToString();
                txt_music_price.Text = row.Cells[4].Value.ToString();
                PictureToPictureBox(dgvMusicCD, picMusic,selectedRowMusicCD);   
                txt_music_stock.Text = row.Cells[6].Value.ToString();
                txt_music_sold.Text = row.Cells[7].Value.ToString();
            }
        }

        private void dgvMagazines_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                DataGridViewRow row = dgvMagazines.Rows[e.RowIndex];
                selectedRowMagazines = Convert.ToInt32(row.Cells[0].Value);                
                txt_mag_name.Text = row.Cells[1].Value.ToString();
                txt_mag_issue.Text = row.Cells[2].Value.ToString();
                combo_mag_type.Text = row.Cells[3].Value.ToString();
                txt_mag_price.Text = row.Cells[4].Value.ToString();
                PictureToPictureBox(dgvMagazines , picMag,selectedRowMagazines);  
                txt_mag_stock.Text = row.Cells[6].Value.ToString();
                txt_mag_sold.Text = row.Cells[7].Value.ToString();
            }
        }

        private void dgvBooks_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                DataGridViewRow row = dgvBooks.Rows[e.RowIndex];
                selectedRowBooks = Convert.ToInt32(row.Cells[0].Value);  

                txt_book_name.Text = row.Cells[1].Value.ToString();
                txt_book_author.Text = row.Cells[2].Value.ToString();
                txt_book_publisher.Text = row.Cells[3].Value.ToString();
                txt_book_price.Text = row.Cells[4].Value.ToString();
                txt_book_ISBN.Text = row.Cells[5].Value.ToString();
                txt_book_page.Text = row.Cells[6].Value.ToString();
                txt_book_stock.Text=row.Cells[8].Value.ToString();
                txt_book_sold.Text = row.Cells[9].Value.ToString();

                PictureToPictureBox(dgvBooks, picBooks,selectedRowBooks);   

            }
        }
                
        private void dgvUsers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            if (e.RowIndex != -1)
            {
                DataGridViewRow row = dgvUsers.Rows[e.RowIndex];
                
                selectedRowIndex = e.RowIndex;
                selectedRowUsername = row.Cells[3].Value.ToString();
                txt_u_name.Text = row.Cells[1].Value.ToString();
                txt_u_surname.Text = row.Cells[2].Value.ToString();
                txt_u_username.Text = row.Cells[3].Value.ToString();
                txt_u_password.Text = row.Cells[4].Value.ToString();
                txt_u_address.Text = row.Cells[5].Value.ToString();
                masked_txt_phone.Text = row.Cells[6].Value.ToString();
                checkBox_isAdmin.Checked = Convert.ToBoolean(row.Cells[7].Value);
                
            }
        }

       
    }
}
