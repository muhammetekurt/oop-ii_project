﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_2_OnlineBookStore
{
    class Book : Product
    {
        private decimal ISBNnumber;
        private string author;
        private string publisher;
        private int page;

        /**
        * parametresiz constructor
        */
        public Book() { }

        /**
        * parametreli constructor
        */

        public Book(decimal ISBNnumber, string author, string publisher, int page, string name, int price, byte[] image, int stock, int soldCount, int id)
        {
            this.ISBNnumber = ISBNnumber;
            this.author = author;
            this.publisher = publisher;
            this.page = page;
            this.name = name;
            this.id = id;
            this.price = price;
            this.image = image;
       
        }

        public decimal ISBNNumber { get => ISBNnumber; set => ISBNnumber = value; }
        public string Author { get => author; set => author = value; }
        public string Publisher { get => publisher; set => publisher = value; }
        public int Page { get => page; set => page = value; }
        /**
        * Urunun ismini dondurur
        * @return name
        */
        public override string getProductName()
        {
            return this.name;
        }
        /**
        * Urunun id'sini dondurur
        * @return id
        */
        public override int getProductID()
        {
            return this.id;
        }
        /**
        * Ürünün fiyatını dondurur
        * @return price
        */
        public override decimal getProductPrice()
        {
            return this.price;
        }

        /**
        * Bu fonksiyon book nesnesinin ozelliklerini yazdırır
        */
        public override string printProperties()
        {
            return "ISBN: " + this.ISBNnumber + " \nAuthor: " + this.Author + "\nPublisher: " + this.Publisher + "\nPage: " + this.Page;
        }
    }
}

