﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_2_OnlineBookStore
{
    class Customer
    {       
        private static Customer _user;
        public List<Customer> CLinCustomers = new List<Customer>();
        private int customerId;
        private string name;
        private string lastName;
        private string username;
        private string password;
        private string address;
        private string phone;
        private bool isAdmin;

        /**
        * parametresiz constructor
        */
        public Customer()
        {                
        }
       
        /**
        * parametreli constructor
        */
        public Customer(Customer user)
        {
            this.customerId = user.customerId;
            this.name = user.Name;
            this.lastName = user.lastName;
            this.username = user.Username;
            this.password = user.Password;
            this.address = user.Address;
        }
        /**
        * parametreli constructor
        */
        public Customer(int customerId, string name, string lastName, string username, string address, string phone)
        {
            this.customerId = customerId;
            this.name = name;
            this.lastName = lastName;
            this.username = username;            
            this.address = address;
            this.phone = phone; 
        }

        public  int getCcustomerID()
        {
            return this.customerId;
        }
        public int CustomerId { get => customerId; set => customerId = value; }
        public string Name { get => name; set => name = value; }
        public string Lastname { get => lastName; set => lastName = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Address { get => address; set => address = value; }
        public string Phone { get => phone; set => phone = value; }

        public bool IsAdmin { get => isAdmin; set => isAdmin = value; }

        /**
        * Bu fonksiyon customer nesnesinin özelliklerini yazdırır
        */
        public string printCustomerDetails()
        {
            return "Customer Id: " + this.customerId + "Customer Name: " + this.name + "Customer Surname: " + this.lastName + "Customer Username: " + this.username +
                "Customer Password: " + this.password + "Customer Address: " + this.address + "Customer Phone: " + this.phone + "Customer Admin?: " + this.isAdmin;
        }
         
    }
}
