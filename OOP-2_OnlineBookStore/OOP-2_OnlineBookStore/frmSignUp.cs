﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace OOP_2_OnlineBookStore
{
    public partial class frmSignUp : Form
    {
        string connectionString = "Data Source = SQL5097.site4now.net; Initial Catalog = db_a750b9_projeodevi2; User Id = db_a750b9_projeodevi2_admin; Password = admin2626";
        public frmSignUp()
        {
            InitializeComponent();
            
        }
            
        private void btnSignUp_Click(object sender, EventArgs e)
        {
             
                if(txtPassword.Text!=txtConfirmPassword.Text)
                {
                     MessageBox.Show("Confirmation does not match! Please check again.");
                }
                else if(txtUsername.Text=="" || txtPassword.Text=="")
                {
                    MessageBox.Show("You forgot to fill mandatory fields!");
                }
                else if (txtName.Text == "" || txtSurname.Text == "")
                {
                    MessageBox.Show("Wow! Don't you have a name or surname?");
                }
                else if (txtAddress.Text == "" || txtSurname.Text == "")
                {
                    MessageBox.Show("We'll probably ship the books to nowhere. Please fill the contact informations!");
                }
                else
                {
                SqlConnection sqlCon = new SqlConnection(connectionString);

               
                SqlCommand cmd = new SqlCommand("Select count(*) from Users where user_username= '"+ txtUsername.Text+"'", sqlCon);
               
                sqlCon.Open();
                int result = Convert.ToInt32(cmd.ExecuteScalar());                

                    if (result != 0 )
                    {                    
                    MessageBox.Show(string.Format("Username: {0} already exist. Please, enter different", txtUsername.Text));
                        result = 0;
                    }
                    else
                    {
                    sqlCon.Close();

                    sqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("UserAdd", sqlCon);
                    MessageBox.Show(sqlCon.State.ToString());            
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@user_name", txtName.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@user_surname", txtSurname.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@user_username", txtUsername.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@user_password", txtPassword.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@user_phone", txtPhoneMasked.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@user_address", txtAddress.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@isAdmin", 0);
                    sqlCmd.ExecuteNonQuery();
                    MessageBox.Show("Registration Succesfully");
                    sqlCon.Close();
                    }
                    
                }     
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            frmLogin frm_login = new frmLogin();
            frm_login.Show();
            this.Hide();
        }
    }
}
