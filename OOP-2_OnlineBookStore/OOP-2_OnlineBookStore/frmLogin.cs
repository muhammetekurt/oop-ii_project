﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;                    

namespace OOP_2_OnlineBookStore
{
    public partial class frmLogin : Form
    {

        LoginedCustomer LC;

        string connectionString = "Data Source = SQL5097.site4now.net; Initial Catalog = db_a750b9_projeodevi2; User Id = db_a750b9_projeodevi2_admin; Password = admin2626";
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {            
            frmSignUp frm_signup = new frmSignUp();
            frm_signup.Show();
            this.Hide();

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();   
        }
              
        private void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            
            SqlConnection con = new SqlConnection(connectionString);
            string login_query = "SELECT * FROM Users WHERE user_username='" + username + "' and user_password= '" + password + "'";
            
            SqlDataAdapter sda = new SqlDataAdapter(login_query, con);
            DataTable datatable = new DataTable();                        
            sda.Fill(datatable);  
            con.Close();  
            
            if(datatable.Rows.Count==1)
            {
                SqlCommand cmd; 
                con.Open();
                cmd = new SqlCommand("select isAdmin from Users Where user_username = '" + username + "'");
                cmd.Connection = con;
                string isAdmin= cmd.ExecuteScalar().ToString();
                con.Close();
                MessageBox.Show("Welcome " + username);
                
                if (isAdmin == "False")
                {     
                    con.Open();
                    cmd = new SqlCommand("select user_id from Users Where user_username = '" + username + "'");
                    cmd.Connection = con;
                    string user_id = cmd.ExecuteScalar().ToString();
                    int id=Convert.ToInt32(user_id);
                    con.Close();

                    con.Open();
                    cmd = new SqlCommand("select user_name from Users Where user_username = '" + username + "'");
                    cmd.Connection = con;
                    string user_name = cmd.ExecuteScalar().ToString();
                    con.Close();

                    con.Open();
                    cmd = new SqlCommand("select user_surname from Users Where user_username = '" + username + "'");
                    cmd.Connection = con;
                    string user_surname = cmd.ExecuteScalar().ToString();
                    con.Close();

                    con.Open();
                    cmd = new SqlCommand("select user_username from Users Where user_username = '" + username + "'");
                    cmd.Connection = con;
                    string user_username = cmd.ExecuteScalar().ToString();
                    con.Close();

                    con.Open();
                    cmd = new SqlCommand("select user_address from Users Where user_username = '" + username + "'");
                    cmd.Connection = con;
                    string user_address = cmd.ExecuteScalar().ToString();
                    con.Close();

                    con.Open();
                    cmd = new SqlCommand("select user_phone from Users Where user_username = '" + username + "'");
                    cmd.Connection = con;
                    string user_phone = cmd.ExecuteScalar().ToString();
                    con.Close();

                    con.Open();
                    SqlCommand sqlCmd = new SqlCommand("DELETE FROM Siparis", con);
                    sqlCmd.ExecuteNonQuery();
                    con.Close();

                    Customer customer =new Customer(id, user_name, user_surname, user_username, user_address, user_phone);

                    LC = LoginedCustomer.Singleton(customer);

                    MainForm frm_dashboard = new MainForm();
                    this.Hide();
                    frm_dashboard.Show();
                    
                }
                else
                {
                    frmAdminPanel frm_adminpanel = new frmAdminPanel();
                    this.Hide();
                    frm_adminpanel.Show();
                }
            }

            else
            {                
                lblNotification.Text="Username or password is wrong!";
            }
          
        }

        
    }
}
