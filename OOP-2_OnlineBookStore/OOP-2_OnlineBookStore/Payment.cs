﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Customer a = new Customer();                     // Bu yorum satırı, giriş yapan kullanıcıya erişmek için kullanılmaktadır.
// MessageBox.Show(a.CLinCustomers[0].Name);
namespace OOP_2_OnlineBookStore
{

    public partial class Payment : Form
    {
        
        string connectionString = "Data Source = SQL5097.site4now.net; Initial Catalog = db_a750b9_projeodevi2; User Id = db_a750b9_projeodevi2_admin; Password = admin2626";
        LoginedCustomer LC;
        int TotalAmount;
        public Payment()
        {
            LoginedCustomer LC = LoginedCustomer.Singleton();
            InitializeComponent();
        }
        
        private void btnBack_Click(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("DELETE FROM Siparis", sqlCon);
            sqlCmd.ExecuteNonQuery();
            sqlCon.Close();

            MainForm main = new MainForm();
            this.Hide();
            main.Show();

        }

        private void Payment_Load(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("Select SUM(quantity*price) from Siparis", sqlCon);
            TotalAmount = Convert.ToInt32(sqlCmd.ExecuteScalar());
            sqlCmd.ExecuteNonQuery();
            sqlCon.Close();
            label1.Text = TotalAmount.ToString();     

            sqlCon.Open();
            SqlCommand cmd = new SqlCommand("Select name, price,quantity,price*quantity AS 'Total Price' from Siparis", sqlCon);
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            dgvOrderlist.DataSource = dt;                 
            sqlCon.Close();

        }
        private void button1_Click(object sender, EventArgs e)
        {
            LoginedCustomer LC = LoginedCustomer.Singleton();
            txtAddress.Text=LC.User.Address;
        }
        private void txtAddress_TextChanged(object sender, EventArgs e)
        {}

        private void btnOrder_Click(object sender, EventArgs e)
        {
            LoginedCustomer LC = LoginedCustomer.Singleton();
            
            if(txtAddress.Text=="")
            {
                MessageBox.Show("Please enter an ADDRESS to ORDER...");
            }

            else if (comboBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Please choose a PAYMENT TYPE(CASH or CREDIT CARD) to ORDER...");
            }

            else 
            {
                SqlConnection sqlCon = new SqlConnection(connectionString);
                sqlCon.Open();
                SqlCommand sqlCmd = new SqlCommand("FinalPrint", sqlCon);
                MessageBox.Show(sqlCon.State.ToString());
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@payment_type", comboBox1.SelectedItem.ToString());
                sqlCmd.Parameters.AddWithValue("@address", txtAddress.Text.Trim());
                sqlCmd.Parameters.AddWithValue("@user_id", LC.User.getCcustomerID());
                sqlCmd.Parameters.AddWithValue("@totalAmount", Convert.ToInt32(TotalAmount));
                sqlCmd.ExecuteNonQuery();
                MessageBox.Show("Your order is preparing...");
                sqlCon.Close();
                MessageBox.Show("Your order is preparing...");
            }

            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("DELETE FROM Siparis", sqlCon);
            sqlCmd.ExecuteNonQuery();
            sqlCon.Close();

            frmLogin login = new frmLogin();
            this.Hide();
            login.Show();
        }
    }
}
