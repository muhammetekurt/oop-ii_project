﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_2_OnlineBookStore
{
    public partial class Entrance : Form
    {
        public Entrance()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            panel2.Width += 3;
            string  loading = " % " + (panel2.Width / 6);
            label1.Text = loading;

            if (panel2.Width>=600)
            {
                timer1.Stop();
                frmLogin form = new frmLogin();
                form.Show();
                this.Hide();

            }
        }
    }
}
