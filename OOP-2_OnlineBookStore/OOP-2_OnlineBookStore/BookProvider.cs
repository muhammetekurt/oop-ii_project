﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Drawing;

namespace OOP_2_OnlineBookStore
{

    class BookProvider
    {
        SqlConnection sqlCon;
        SqlCommand cmd;
        string query;

        string connectionString = "Data Source = SQL5097.site4now.net; Initial Catalog = db_a750b9_projeodevi2; User Id = db_a750b9_projeodevi2_admin; Password = admin2626";

        /**
        * parametresiz constructor
        */
        public BookProvider()
        {
            sqlCon = new SqlConnection(connectionString);

        }
        /**
        * Bu fonksiyon Veritabanından Books urunlerinin degerlerini okuyarak olusturulan BookList icerisine kaydediyor.
        */
        public List<Book> booklistFromDatabase()  
        {
            List<Book> BookList = new List<Book>();
            query = "SELECT id,name, author, publisher, price, ISBN,page, images,stock, soldCount FROM Product where product_type=1";
            sqlCon.Open();
            cmd = new SqlCommand(query, sqlCon);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Book book = new Book(); 

                book.id= Convert.ToInt32(dr[0]);        
                book.name = dr[1].ToString();           
                book.Author = dr[2].ToString();
                book.Publisher = dr[3].ToString();
                book.price = Convert.ToInt32(dr[4]);
                book.ISBNNumber = Convert.ToDecimal(dr[5]);
                book.Page = Convert.ToInt32(dr[6]);


                byte[] picture = new byte[0];  
                
                if (dr[7] == DBNull.Value)
                {
                     picture = null;
                   
                }
                else
                {                    
                    picture = (byte[])dr[7];                    
                }


                book.image = picture;
                book.stock = Convert.ToInt32(dr[8]);
                book.soldCount = Convert.ToInt32(dr[9]);

                BookList.Add(book);   

            }
            sqlCon.Close();

            return BookList;
        }

    }
}

