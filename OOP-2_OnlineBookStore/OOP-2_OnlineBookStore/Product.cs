﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_2_OnlineBookStore
{
    /**
    * Bu class bir abstract classdır.
    */
    abstract class Product
    {
        public string name { get; set; }
        public int id { get; set; }
        public decimal price { get; set; }
        public byte[] image { get; set; }
        public int stock { get; set; }
        public int soldCount { get; set; }

        public abstract string getProductName();
        public abstract int getProductID();
        public abstract decimal getProductPrice();
        public abstract string printProperties();
    }
}
