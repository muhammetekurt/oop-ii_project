﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_2_OnlineBookStore
{
    class ItemToPurchase
    {
        private Product product;
        private int quantity;
        /**
        * parametresiz constructor
        */
        public ItemToPurchase()
        {

        }
        /**
        * parametreli constructor
        */
        public ItemToPurchase(Product _product, int _quantity)
        {
            product = _product;
            quantity = _quantity;
        }

        public List<ItemToPurchase> ShoppingCartGetter(List<ItemToPurchase> a)
        {
            List<ItemToPurchase> b = new List<ItemToPurchase>();
            b = a;
            return b;
        }

        public Product Product { get => product; set => product = value; }
        public int Quantity { get => quantity; set => quantity = value; }
    }

}