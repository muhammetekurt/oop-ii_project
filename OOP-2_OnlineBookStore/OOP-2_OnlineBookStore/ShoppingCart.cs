﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_2_OnlineBookStore
{
    class ShoppingCart
    {
        public List<ItemToPurchase> ItemsToPurchase;
        private int customerId;        
        string paymentType;
        int totalAmount;

        public ShoppingCart()
        {
            ItemsToPurchase = new List<ItemToPurchase>();
        }
       
        //public ItemToPurchase ItemsToPurchase { get => itemsToPurchase; set => itemsToPurchase = value; }

        public int CustomerId { get => customerId; set => customerId = value; }

        public string PaymentType { get => paymentType; set => paymentType = value; }

        public void sendInvoicebySMS()
        {

        }

        public void addProduct()
        {
 
        }

        public ShoppingCart(int _customerId)
        {
            totalAmount = 0;
            customerId = _customerId;           
        }

    }

}
