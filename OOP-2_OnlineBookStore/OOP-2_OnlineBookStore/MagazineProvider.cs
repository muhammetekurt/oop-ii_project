﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Drawing;

namespace OOP_2_OnlineBookStore
{

    class MagazineProvider
    {
        SqlConnection sqlCon;
        SqlCommand cmd;
        string query;

        string connectionString = "Data Source = SQL5097.site4now.net; Initial Catalog = db_a750b9_projeodevi2; User Id = db_a750b9_projeodevi2_admin; Password = admin2626";

        public MagazineProvider()
        {
            sqlCon = new SqlConnection(connectionString);

        }
        /**
        * Bu fonksiyon veritabanından Magazine ürünlerinin değerlerini okuyarak oluşturulan MagazineList içerisine kaydediyor
        * 
        */
        public List<Magazine> magazinelistFromDatabase()  
        {
            List<Magazine> MagazineList = new List<Magazine>();            
            query = "SELECT id, name, issue, type, price,images, stock, soldCount FROM Product where product_type=2";
            sqlCon.Open();
            cmd = new SqlCommand(query, sqlCon);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Magazine mag = new Magazine(); 

                mag.id = Convert.ToInt32(dr[0]);        
                mag.name = dr[1].ToString();          
                mag.Issue = Convert.ToInt32(dr[2]);
                mag.Type = dr[3].ToString();
                mag.price = Convert.ToInt32(dr[4]);
                
                byte[] picture = new byte[0];  

                if (dr[5] == DBNull.Value)
                {
                    picture = null;                    
                }
                else
                {                    
                    picture = (byte[])dr[5];                    
                }


                mag.image = picture;

                int stc;
                if (dr[6] == DBNull.Value)
                {
                    stc = 0;                                   
                }
                else
                {                    
                    stc = Convert.ToInt32(dr[6]);                  
                }
                mag.stock = stc;

                int cnt;
                if (dr[7] == DBNull.Value)
                {
                    cnt = 0;
                }
                else
                {
                    cnt = Convert.ToInt32(dr[7]);
                }
                mag.soldCount = cnt;
                

                MagazineList.Add(mag);   

            }
            sqlCon.Close();

            return MagazineList;
        }

    }
}
