﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Drawing;

namespace OOP_2_OnlineBookStore
{
    class CustomerProvider
    {
        SqlConnection sqlCon;
        SqlCommand cmd;
        string query;
       
        string connectionString = "Data Source = SQL5097.site4now.net; Initial Catalog = db_a750b9_projeodevi2; User Id = db_a750b9_projeodevi2_admin; Password = admin2626";
        /**
        * Parametresiz constructor
        */
        public CustomerProvider()
        {
            sqlCon = new SqlConnection(connectionString);

        }
        /**
        * Bu fonksiyon Veritabanından Customer degerlerini okuyarak oluşturulan Customer Listesi icerisine kaydediyor.
        */
        public List<Customer> customerlistFromDatabase()  
        {
            List<Customer> customerlist = new List<Customer>();
            
            query = "SELECT user_id, user_name,user_surname, user_username, user_password, user_address,user_phone,isAdmin FROM Users";
            sqlCon.Open();
            cmd = new SqlCommand(query, sqlCon);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {               
                Customer cs = new Customer(); 

                cs.CustomerId = Convert.ToInt32(dr[0]);        
                cs.Name = dr[1].ToString();           
                cs.Lastname = dr[2].ToString();
                cs.Username = dr[3].ToString();
                cs.Password = dr[4].ToString();
                cs.Address = dr[5].ToString();
                cs.Phone = dr[6].ToString();
                cs.IsAdmin = Convert.ToBoolean(dr[7]);                 

                customerlist.Add(cs);   
            }
            sqlCon.Close();

            return customerlist;
        }
                 

    }
}
