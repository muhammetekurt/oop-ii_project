﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_2_OnlineBookStore
{
    
    class MusicCD : Product
    {
        private string singer;
        private string type;
        /**
        * parametresiz constructor
        */
        public MusicCD() { }
        /**
        * parametreli constructor
        */
        public MusicCD(string singer, string type, string name, int price, byte[] image, int stock, int soldCount, int id)
        {
            this.singer = singer;
            this.type = type;
            this.name = name;
            this.id = id;
            this.price = price;
            this.image = image;
            this.stock = stock;
            this.soldCount = soldCount;
        }

        public string Type { get => type; set => type = value; }
        public string Singer { get => singer; set => singer = value; }
        /**
        * Ürünün ismini döndürür
        * @return name
        */
        public override string getProductName()
        {
            return this.name;
        }
        /**
        * Ürünün id sini döndürür
        * @return id
        */
        public override int getProductID()
        {
            return this.id;
        }
        /**
        * Ürünün fiyatını döndürür
        * @return price
        */
        public override decimal getProductPrice()
        {
            return this.price;
        }
        /**
        * Bufonksiyon ürünün özelliklerini yazdırır
        */
        public override string printProperties()
        {
            return "Singer: " + this.singer + " \nType:: " + this.type;
        }

        
    }
}
